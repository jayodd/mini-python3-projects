# Read Me

This is a repo with some of the off-platform mini projects from CodeAcademy.com's Python 3 and SQLite courses on data visualisation and analysis.

In my free time, I like to expand my skill set. These projects have helped me to do this.

The majority of these (with *.ipynb) were written in a Jupyter Notebook, hence this offers the best viewing experience.

